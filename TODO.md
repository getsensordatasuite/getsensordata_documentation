# TODO

## Wi-Fi Data Timestamps

Task:

- Check again if Android's last seen field contains useful data.

Question:

> I have a quick question about WiFi data from the 2019 track files (better late than never J)
I was wondering what timestamp of the WiFi reading correspond to ?
Does this timestamp correspond to when the WiFi scan has started or when it  has ended ? (a full scan take anywhere between 2-4 seconds)
>
> Moreover, for the best of my knowledge Android WiFi scan API provides list of results, where every AP has its own “last seen” timestamp.
However, some of the results in this list can be outdated (for example 5 seconds ago).
Do you take this into account ? Or you just provided us with everything that API call gave you ?
>
> Is the source code of you logger app publicly available to look on these nuances ?

Answer:

> If I remember correctly, we timestamp WiFi measured when android send us a notification, just after full scanning is completed. The "last seen" field did not contaiened useful data, as we concluded last time we tested it a couple of years ago (maybe new android versions, in some phones, could include usefult data about the real measured instant of time. Not studied recently).
>
> You can download apk https://lopsi.weebly.com/downloads.html and sources from our Gitlab repository:  https://www.car.upm-csic.es/lopsi/GetSensorDataSuite  (and also contribute to improve it, if wished. Repository maintained by Andy in CC). If after reading the code, and your own experimentation, you can find a better answer to your questions, please share them with us. It would be good to know.

## Background operation

- **Goal**: App still recording data even if in the background, or with the screen switched-off.
- **Current state**: App only records data into the logfile when screen in on and running in the foreground.
- **Implementation ideas**: Use a background service that  is always running.

## On-line data streaming

- **Goal**: Send real time data (same as in log file) to a server computer running Matlab.
- **Current state**: App does not stream any data at all.
- **Implementation ideas**: UDP comunications sharing IP adresses (phone and computer).
- **References**: At Linkoping university they have a similar App with this on-line streaming already implemented. App: https://play.google.com/store/apps/details?id=com.hiq.sensor&hl=es_419 . Some use examples in Matlab: http://www.sensorfusion.se/sfapp/sf-app-examples/  and a link to download java classes used in Matlab: https://goo.gl/DGBSOC (here the hierarchy of classes: http://sensorfusion.se/assets/javadoc/index.html).

## GNSS RAW

- **Goal**: Add GNSS RAW pseudorange measurements for all constellations (GPS, Glonass, Galileo and Beidu)
- **Current state**: Only the position GNSS fix is stored, but not the pseudoranges and other RAW data.
- **Implementation ideas**: Use the GNSS android App from Google and Developer studio information to implement it (https://developer.android.com/guide/topics/sensors/gnss)

## BLE Beacon Mode

- **Goal**: Allow the app to enable BLE Beacon Mode, turning the device into a BLE Beacon that can be measured from other smartphones.
- **Current state**: Not defined.
- **Implementation ideas**: Two alternatives:
    - include this functionality in GetSensorDataAndroid.
    - create a new app belonging to GetSensorDataSuite that can provide this functionality on its own.

## Avoid Race Conditions

- **Goal**: Let the app determine if sensor resources are under stress if asked to perform certain task faster than they could. For example, avoid taking samples with the camera faster than they can be acquired. In that particular instances, the app could even crash when the camera is asked to take one sample before finishing taking the current one.
- **Current state**: There are no current measures in place to avoid or even mitigate this condition.
- **Implementation ideas**: At least, we should find a way to keep track of this circumstance, to see if the data collection is lagging behind what is expected. Ideally, we should be able to adjust these delays intelligently, warning the user about them. If we take one sample per second and the system needs two seconds to perform the task, the app should notice it and inform the user, so that he can take the necessary measures.

In view of this, there could be an option for the adjustment of the sampling time to be automatic. It would also be nice to have a visual element of some kind that shows how the data acquisition is going, so that, at a glance, we can know if there are any problems.

## Title

- **Goal**:
- **Current state**:
- **Implementation ideas**:
