# Contributing with GetSensorDataSuite

## Table of Contents

1. [Introduction](#introduction)
2. [Why Git](#why-git)
3. [Why GitLab](#why-gitlab)
4. [Brief Introduction to Git](#brief-introduction-to-git)
5. [Branch Management](#branch-management)
6. [Version Control Tool Configuration](#version-control-tool-configuration)
    1. [Obtaining a Remote Repository](#obtaining-a-remote-repository)
7. [Creating a GitLab Account](#creating-a-gitlab-account)
8. [Using Android Studio with GitLab](#using-android-studio-with-gitlab)
    1. [Branch Selection](#branch-selection)
    2. [Windows](#windows)
9. [GetSensorDataSuite Workflow](#getsensordatasuite-workflow)

## Introduction

Interested in collaborating on GetSensorDataSuite? The first step you should take is familiarizing yourself with the project. GetSensorDataSuite development is managed using Git. The central repositories are located in GitLab:

- [**Group of projects**][getsensordata]: contains all related repositories.
- [**GetSensorData_Android**][getsensordata_android]: Android app that captures all sensors data.
- [**GetSensorData_Matlab**][getsensordata_matlab]: MATLAB scripts capable of analyzing the data captured via the Android app.
- [**GetSensorData_Documentation**][getsensordata_documentation]: project documentation. You're probably reached this document via said repository.

## Why Git

Because it is essential to have a version control system while developing a project of these characteristics,with the participation of members from different geographical locations. Although Git is not a server-dependent version control system, it does allow for central repositories.

> **Note:** Don't worry if you haven't worked with Git before, you will find an introduction to it in this same document.

## Why GitLab

While there are many other alternatives -- GitHub or Bitbucket, to name a few --, GetSensorDataSuite is hosted on GitLab because:

- **it's free**, at least for our current needs.
- **is open**, which means that if, for some reason, we are no longer interested in having the project hosted on their servers, we have the possibility of installing our own copy of GitLab on a server and continue working from there.
- **we can create groups**, which allows us to better organize the work and prepare for a future in which we publish more projects in addition to this one.

Although there are many online resources available to learn how to use Git, below is a brief guide that explains both the basics of Git and the different and particular rules we use in this project.

## Brief introduction to Git

Git is a tool created by Linus Torvalds. Although his initial intention was to manage versions of files containing software source code, it can be used with any text file. Git can also be used with binary files, but it does not show the differences between them.

Any application we use to manage version control with Git ends up calling a command line application called **git**, so the best way to contact Git is without any intermediaries. First of all, install Git on your computer if you haven't already. This [guide][git_install] may help you do this on your operating system.

Follow these steps to start using Git:

1. Open a terminal where you can use Git.

2. Create a temporary folder to work in. On your desktop, for example:

    ```bash
    cd ~/Desktop
    mkdir hello
    cd hello
    ```

3. Following Git nomenclature, each project with version control active is known as a _repository_. Turn that folder into a local Git repository:

    ```bash
    git init
    ```

    The folder you just created should be empty. However, after converting it to a Git repository there will be a hidden folder named **.git**. Git keeps in it everything needed to manage the repository versions. You can take a look at its contents, but be careful! You shouldn't touch anything in there.

    ![**.git** folder contents][git_contents]

4. Create a new file called **hello.md** inside the new folder with the content `Hello!`.

5. This new file you just created is not yet under version control. You must add it to the repository for Git to take it into account:

    ```bash
    git add hello.md
    ```

6. There is only one more step left for the file to be within version control, you have to confirm the change:

    ```bash
    git commit
    ```

    Git will show you a window describing the changes. These descriptions are very important, even if it doesn't seem so. You should follow Chris Beams' instructions in his fantastic article ["How to Write a Git Commit Message"][commit_message] (of which there is a [Spanish version][commit_message_es]). In short: start with an infinitive verb and describe what that change does in less than 50 characters. If you need more space you can add a blank line and expand below. But don't exceed 72 characters per line in the extended explanation. Something like that:

    ![Commiting a change][git_commit]

7. Ah, but what would a guide like this be without the typical "Hello, World!" example. Change the content of the file you just created so that, instead of "Hello!" be "Hello, World!"

8. Use the `git status` command to verify that something has changed.

    ![Did anything change?][git_status]

    As you can see, Git says that something has changed, but that change has not yet been added to the repository.

9. Add the change to the repository with `git add hello.md` and confirm with `git commit`, properly describing the change. It can be done in the same line:

    ```bash
    git commit -m "Greet the world"
    ```

10. Take a look at the log of changes made:

    ```bash
    git log --oneline
    ```

    ![Change log][git_log]

    The log shows the changes in reverse chronological order, with the most recent at the top.

11. Each line begins with the identifier of that change, which will allow us to switch between different versions of a file. That way, we can change to an earlier version of the file:

    ```bash
    git checkout cb6f2c6 --quiet
    ```

    Let's check the contents of the file:

    ![Back to the future][git_checkout]

    Notice how we have returned to the most recent version of the file: instead of using its number (which we can also refer to as _hash_) we have used the alias **master**, which points to the most recent version of the file. In your case hash numbers should be different.

There are many Git features not covered in this section, such as branch management or synchronization with a central repository, just to mention the two that most concern us. However, what we've dealt with so far serves as a first contact so you know what Git is and have the basic knowledge of what is happening behind the scenes when you use another user interface.

## Branch management

It's impossible to cover a topic as broad as Git branch management in its entirety in a document of this kind. But, at least, we can establish a simple workflow that can be used in this project. It's slightly based in [A successful Git branching model][branch_management].

The core idea is to leave the main project in the master branch. Every new feature must be developed and tested in a new branch. Only when said feature is complete can it be merged into the master branch. When the merge is completed, the feature branch can be removed from the repository.

But enough talking, let's see how this works with a simple example. We've just created a repository with a text file, **hello.md**. Someone in the team is convinced that we should also ask `How are you today?` to it. In order to do so, follow these steps:

1. Create a new branch called **politeness**:

    ```bash
    git branch politeness
    ```

2. Creating the branch is not enough, you also have to start using it:

    ```bash
    git checkout politeness
    ```

3. Just to be sure which branch you are in use this command:

    ```bash
    git branch
    ```

    The one currently active is marked with an asterisk.

    ![List of branchs][git_branch]

4. Proceed as usual, making your changes, adding, and commiting them. For example, after changing and saving **hello.md** you will have to use this commands:

    ```bash
    git add hello.md
    git commit -m "Ask the world how does it feel"
    ```

5. If you are happy about your changes, now is time to merge them back into the master branch. First, leave the branch **politeness** and check **master** out:

    ```bash
    git checkout master
    ```

6. Now, tell Git to merge the new branch back into the master one:

    ```bash
    git merge politeness
    ```
    ![Merge into master][git_merge]

7. If the merge is successful, the commit messages of the feature branch will appear in the master one:

    ![Log after merge][git_merge_log]

8. To leave everything as clean as possible, after a successful merge operation you should remove the feature branch:

    ```bash
    git branch -d politeness
    ```

9. Take a look again at the logs:

    ![Log after branch deletion][git_branch_delete]

> **Note:** There are a couple of points that we need to explain yet if we want to master this branching workflow in our team: pushing branches to the main repository and managing merge conflicts. These topics will be covered in a future revision of this document.

## Version Control Tool Configuration

Although we can use Git from the command line, it is sometimes more convenient to use a tool with a more friendly interface. Most development environments have version control integrated, and there are not much differences in their operation.

Let's see how to use what is possibly the most used application to deal with Git, Sourcetree. Developed by Atlassian, it is free and works on Windows and macOS. What we will see applies to almost all version control tools compatible with Git.

Follow these steps to carry out with Sourcetree the same operations that we performed previously from the command line:

1. Download and install [Sourcetree][sourcetree].
2. Launch Sourcetree. First you will be asked to create an account on the Atlassian website. Click on the **Create a free account today!** link at the bottom of the window.

    ![On Sourcetree first launch][sourcetree_first_execution]

3. After creating the account you should return to Sourcetree automatically and see a congratulations message. If not, do it manually and click on the **Bitbucket Cloud** button, which should take you to Sourcetree again after going through the Web client.

    ![Account created][sourcetree_registered]

4. Sourcetree will now ask you to enter the details you want to appear every time you perform an operation with Git. Enter the name you want other users to see, as well as your email address.

    ![Preferences][sourcetree_preferences]

5. When the setup process is complete you will see Sourcetree main window. Click on the **Local** button located in the upper left corner.

    ![Local repositories][sourcetree_local]

Now you have a tool with a friendlier user interface at your disposal. Let's perform the same operations we did before in the terminal:

1. **Create a local repository**. Click on the **New** button, at the top of the main Sourcetree window, and then select the **Create Local Repository** option. A dialog box will appear:

    ![Create a local repository][sourcetree_create_local_repository]

    Enter the requested values and click on **Create**. Sourcetree main window will then display the new repository.

    ![Local repository created][sourcetree_local_repository]

2. **Add a new file to the repository**. In the previous section the file was named **hello.md** and its initial content was `Hello!`. When you do this, the Sourcetree main window will reflect that change by showing a 1 inside a circle in the repository you just created.

    ![Local repository modified][sourcetree_local_repository_modified]

3. **Double click on the repository to see the changes**. You will see that a new file has been added and its contents.

    ![Local repository changes][sourcetree_local_repository_view_changes]

4. **Confirm your changes**. Mark the checkbox next to the name of the newly created file and write a message that describes those changes:

    ![Commit local repository changes][sourcetree_local_repository_commit_changes]

    When finished, click on the **Commit** button. Sourcetree will ask you if you want to add the file to the version control system. Answer **OK** and, in addition, mark **Do not show this message again**:

    ![Add file][sourcetree_confirm_add]

    From that moment on, the changes you made will be registered in the repository and will be part of the version control:

    ![Local repository changes, committed][sourcetree_local_repository_committed]

5. **Modify the file**. Change `Hello!` to `Hello, World!`. The repository window will notice that something has changed and will show that there are unconfirmed changes:

    ![New local repository changes][sourcetree_local_repository_changes_2]

    Select **Uncommitted changes** to see what has changed.

    ![New local repository changes, selected][sourcetree_local_repository_view_changes_2_selected]

6. **Confirm you changes**. Click on **File status**, in the left pane, mark the checkbox next to the newly modified file, and write a message describing you changes:

    ![Commit new local repository changes][sourcetree_local_repository_commit_changes_2]

    Click on **Commit** to confirm your changes:

    ![New local repository changes, committed][sourcetree_local_repository_committed_2]

    As you can see, the most recent changes appear first in log, with the oldest changes below.

### Obtaining a remote repository

Now, how to use Sourcetree to download to your computer a repository like the one that contains this document you are reading? Follow these steps:

1. **Clone the remote repository**. To do this, click on the **New** button, at the top of Sourcetree main window, and then select the **Clone from URL** option. A dialog box will appear:

    ![Repository to clone][sourcetree_clone]

    Puedes obtener el URL del repositorio a clonar desde la página web del repositorio.

    The repository URL can be obtained from the repository web page.

2. Click the **Clone** button. Sourcetree will download the repository source code and show it to you in its window:

    ![Cloned repository][sourcetree_cloned]

## Creating a GitLab account

Follow these steps to create a user account in GitLab and configure it to collaborate with GetSensorDataSuite:

1. Visit [GitLab][gitlab] home page and click on the **Register** link, in the upper right corner of the page.

    ![Create a GitLab account][gitlab_register]

2. Fill in the requested fields.

    ![Registration data][gitlab_register_data]

3. Click the **Register** button. GitLab will send a confirmation message to the email address you provided. Your account will not be active until you click on the link **Confirm your account** included in that message.

4. Once you have created the account and logged in, visit the [user settings page][gitlab_user_settings].

    ![User settings][gitlab_user_settings]

5. Customize the **Profile** section. Here you decide what data you want to show the world about yourself. It is good manners to use an avatar that allows the rest of the team members to easily recognize you.

6. Click **SSH Keys**, on the left side of the screen.

    ![Add SSH keys][gitlab_ssh]

    Here you have to provide the public part of the SSH key that you will allow you to send your changes to GitLab. Creating it is as simple as opening a terminal and executing the following command:

    ```bash
    ssh-keygen -t ed25519 -C "user@example.com" -f $HOME/.ssh/lopsi_ed25519
    ```

    Replace `user@example.com` with the email address you used to sign up for GitLab. In your user folder, inside the folder **.ssh**, there will be a file called **lopsi_ed25519**: don't share it with anyone, it's your private key. You will also see a file called **lopsi_ed25519.pub**: this is your public key. Copy its contents and paste them into the **key** text box on the GitLab page you opened earlier.

    ![GitLab public key][gitlab_public_key]

    Click on the **Add key** button when you finish. GitLab will show you the key data you just added.

    ![Public key data on GitLab][gitlab_public_key_data]

7. Contact us to add you to the team. In order for us to do this you just have to indicate the username of the GitLab account you've just created. The more information about the account you provide (such as your full name or email address), the easier it will be for us to find you in case there is any doubt.

> **Note:** If that's they only SSH key you're going to use, name it **id_ed25519** and **id_ed25519.pub**. That way, the SSH agent will know that's the default one.

## Using Android Studio with GitLab

Perhaps the fastest way to start working on GetSensorData_Android is to use the development environment itself for version control management.

> **Note:** Always use the latest version of Android Studio. The reasons are many: it solves problems from previous versions, includes improvements, allows working with the latest versions of Android, and so on. The downside is that it may be necessary to modify the project in some way. We will always try to make the project code compatible with the latest version of Android Studio.

Follow the steps below to get the latest version of the GetSensorData_Android code using Android Studio:

1. Install [Android Studio][android_studio].
2. Launch Android Studio. You will see a welcome screen.

    ![Android Studio Welcomes you][android_studio_welcome]

3. Click **Check out project from Version Control**. A drop-down list will appear: select **Git** to display the **Clone Repository** dialog box.

    ![Android Studio Clone Repository][android_studio_clone_repository]

4. Enter the URL of the project you want to clone in the **URL** text box. Click the **Test** button to verify that the URL entered is correct. Select the folder in which you want to save the project code by clicking on the folder-shaped button to the right of the **Directory** text box.

    ![Android Studio Clone Repository, ready][android_studio_clone_repository_ready]

5. Click the **Clone** button to download the code. Android Studio will show you the download progress in a dialog box.

    ![Android Studio Cloning Repository][android_studio_cloning_repository]

6. When Android Studio finishes cloning the repository it will ask you if you want to create a new project from what you have downloaded. Click the **No** button.

    ![Android Studio, asking to create a new project][android_studio_create_project]

7. You will see Android Studio's welcome dialog again. This time select the option **Import project (Gradle, Eclipse ADT, etc.)**. A dialog box will appear, use it to select the **GetSensorData20** folder inside the repository you just cloned. Then, click on the **Open** button.

    ![Android Studio import project][android_studio_import_project]

8. In case there is any problem importing the project, Android Studio will inform you about it, offering methods to solve it. For example, you may not have the necessary SDK installed. In that case you will have to click on the link that provided to install it. It may ever offer you to update Gradle: say yes.

9. You're done! The project is ready to work on it.

> **Note:** Do not add to version control the files generated by the development environment. They usually depend on your configuration -- including local routes that may not be available on the equipment of the rest of the group members --. It may even be temporary files -- such as binaries resulting from a compilation --. GetSensorData_Android project includes a **[. Gitignore][gitignore]** file obtained from [the collection provided by GitHub][github_gitignore], but with a small modification: it ignores everything in the hidden folder **.idea**.

![Android Studio project configuration's files][android_studio_project_configurations_files]

### Branch selection

You may want to work in a different branch than the main -- or master -- branch. To do this you just have to follow these steps:

1. Open the project.

2. Select **VCS > Git > Branches**.

    ![Android Studio branches][android_studio_branches]

3. Notice there is an alert triangle in the upper right corner of the dialog box that will appear. It means that Android Studio is not sure it's working with the latest repository data. Click on said triangle to update the data. Repeat this operation whenever you see that warning.

4. Select **VCS > Git > Branches** again. The warning should no longer appear. The star to the left of **origin / master** means that this is the branch we are working with now.

    ![Android Studio current branch][android_studio_current_branch]

5. Let's change the current branch. Click on **origin / cleanup** option and select the **Checkout As** to work with that branch. Do not change the name of the branch -- leave the value provided -- and click **OK**. Android studio will notify you when the process is finished.

    ![Android Studio branch changed][android_studio_branch_changed]

### Windows

If you're using Windows, the above steps are a little bit more complicated, but not much more. First of all, you need to install Git and setup the SSH key. Follow these steps in order to do so:

1. Install [Git for Windows][git_for_windows]. Make sure to select all components. TrueType will improve the console appearance, while checking for Git updates will allow you to have Git last version as soon as it is released.

    ![Git for Windows components][git_for_windows_components]

2. Click with your right mouse button on the desktop and select the option **Git Bash Here**.

3. Create your SSH key:

    ```bash
    ssh-keygen -t ed25519 -C "user@example.com"
    ```

    Replace `user@example.com` with the email address you used to sign up for GitLab. Accept the defaults. As a result, you will have a new private key in a file called **id_ed25519**, with its correspondent public key called **id_ed25519.pub** in the folder **~/.ssh/**.

4. If you already have your SSH key, type `cd` to go to your user's directory.

5. Check if the folder `.ssh` exists there. If not, create it.

    ![Does ~/.ssh folder exists?][git_for_windows_ssh_folder]

6. Copy your public and private keys there. If that's the only key you're using, change its name to **id_ed25519** and **id_ed25519.pub**. That way, the SSH agent will know that's the default one.

Now you have Git and the SSH key in place you can follow the same steps described before, with a minor difference. When you fill the **Clone Repository** dialog box and click **Test** for the first time, Android Studio will tell you that the authenticity of GitLab's domain can not be confirmed. What this means is that it is not in the list of known hosts SSH maintains. Click **Yes** to add that domain to the list. Next time it won't bother you with this notice.

![GitLab domain is not know... yet][git_for_windows_gitlab_authenticity]

## GetSensorDataSuite workflow

A simplified version of this project's workflow could be:

1. Obtain the SSH URL from the repository.
2. Clone the project.
3. Create a local branch to make changes.
4. Confirm your changes.
5. Push them.
6. If all went well, merge them with the main branch and remove the branch you created.

As we work on the project we will expand this section.

The idea is to be organized but work at ease. The rules we propose are the following, but we don't have to worry if we make a mistake and we don't comply with them, everything can be fixed because we're a team and we are using version control.

- **[Branch management][branch_management]**: a simplified version of it, of course.
- **[Version numbers][semantic_versioning]**
- **[Commit messages][commit_message]**: [also in Spanish][commit_message_es].

[getsensordata]: https://gitlab.com/getsensordatasuite "Group of projects"
[getsensordata_android]: https://gitlab.com/getsensordatasuite/getsensordata_android "GetSensorData_Android"
[getsensordata_matlab]: https://gitlab.com/getsensordatasuite/getsensordata_matlab "GetSensorData_Matlab"
[getsensordata_documentation]: https://gitlab.com/getsensordatasuite/getsensordata_documentation "GetSensorData_Documentation"

[git_install]: https://www.atlassian.com/git/tutorials/install-git "How to install Git on macOS, Windows, and Linux"
[commit_message]: https://chris.beams.io/posts/git-commit/ "How to Write a Git Commit Message"
[commit_message_es]: http://tomasdelvechio.github.io/old/440/ "Cómo escribir un mensaje de commit de git"

[gitlab]: https://gitlab.com/ "GitLab is a complete DevOps platform, delivered as a single application."
[gitlab_user_settings_url]: https://gitlab.com/profile?nav_source=navbar "GitLab User Settings"

[sourcetree]: https://www.sourcetreeapp.com "Simplicity and power in a beautiful Git GUI"

[git_contents]: figures/git_contents.png "**.git** folder contents"
[git_commit]: figures/git_commit.png "Commiting a change"
[git_status]: figures/git_status.png "Did anything change?"
[git_log]: figures/git_log.png "Change log"
[git_checkout]: figures/git_checkout.png "Back to the future"

[gitlab_register]: figures/gitlab_register.png "Create a GitLab account"
[gitlab_register_data]: figures/gitlab_register_data.png "Registration data"
[gitlab_user_settings]: figures/gitlab_user_settings.png "User settings"
[gitlab_ssh]: figures/gitlab_ssh.png "Add SSH keys"
[gitlab_public_key]: figures/gitlab_public_key.png "GitLab public key"
[gitlab_public_key_data]: figures/gitlab_public_key_data.png "Public key data on GitLab"

[git_branch]: figures/git_branch.png "List of branchs"
[git_merge]: figures/git_merge.png "Merge into master"
[git_merge_log]: figures/git_merge_log.png "Log after merge"
[git_branch_delete]: figures/git_branch_delete.png "Log after branch deletion"

[sourcetree_first_execution]: figures/sourcetree_first_execution.png "On Sourcetree first launch"
[sourcetree_registered]: figures/sourcetree_registered.png "Account created"
[sourcetree_preferences]: figures/sourcetree_preferences.png "Preferences"
[sourcetree_local]: figures/sourcetree_local.png "Local repositories"

[sourcetree_create_local_repository]: figures/sourcetree_create_local_repository.png "Create a local repository"
[sourcetree_local_repository]: figures/sourcetree_local_repository.png "Local repository created"
[sourcetree_local_repository_modified]: figures/sourcetree_local_repository_modified.png "Local repository modified"
[sourcetree_local_repository_view_changes]: figures/sourcetree_local_repository_view_changes.png "Local repository changes"
[sourcetree_local_repository_commit_changes]: figures/sourcetree_local_repository_commit_changes.png "Commit local repository changes"
[sourcetree_confirm_add]: figures/sourcetree_confirm_add.png "Add file"
[sourcetree_local_repository_committed]: figures/sourcetree_local_repository_committed.png "Local repository changes, committed"
[sourcetree_local_repository_changes_2]: figures/sourcetree_local_repository_changes_2.png "New local repository changes"
[sourcetree_local_repository_view_changes_2_selected]: figures/sourcetree_local_repository_view_changes_2_selected.png "New local repository changes, selected"
[sourcetree_local_repository_commit_changes_2]: figures/sourcetree_local_repository_commit_changes_2.png "Commit new local repository changes"
[sourcetree_local_repository_committed_2]: figures/sourcetree_local_repository_committed_2.png "New local repository changes, committed"
[sourcetree_clone]: figures/sourcetree_clone.png "Repository to clone"
[sourcetree_cloned]: figures/sourcetree_cloned.png "Cloned repository"

[gitignore]: https://git-scm.com/docs/gitignore "Specifies intentionally untracked files to ignore"
[github_gitignore]: https://github.com/github/gitignore "A collection of useful .gitignore templates"

[android_studio]: https://developer.android.com/studio "Android Studio provides the fastest tools for building apps on every type of Android device"

[android_studio_welcome]: figures/android_studio_welcome.png "Android Studio Welcomes you"
[android_studio_clone_repository]: figures/android_studio_clone_repository.png "Android Studio Clone Repository"
[android_studio_clone_repository_ready]: figures/android_studio_clone_repository_ready.png "Android Studio Clone Repository, ready"
[android_studio_cloning_repository]: figures/android_studio_cloning_repository.png "Android Studio Cloning Repository"
[android_studio_create_project]: /figures/android_studio_create_project.png "Android Studio, asking to create a new project"
[android_studio_import_project]: /figures/android_studio_import_project.png "Android Studio import project settings"
[android_studio_import_project]: /figures/android_studio_import_project.png "Android Studio import project"
[android_studio_branches]: /figures/android_studio_branches.png "Android Studio branches"
[android_studio_current_branch]: /figures/android_studio_current_branch.png "Android Studio current branch"
[android_studio_project_configurations_files]: /figures/android_studio_project_configurations_files.png "Android Studio project configuration's files"
[android_studio_branch_changed]: /figures/android_studio_branch_changed.png "Android Studio branch changed"

[git_for_windows]: https://git-scm.com/download/win "Git for Windows"
[git_for_windows_components]: /figures/git_for_windows_components.png "Git for Windows components"
[git_for_windows_ssh_folder]: /figures/git_for_windows_ssh_folder.png "Does ~/.ssh folder exists?"
[git_for_windows_gitlab_authenticity]: /figures/git_for_windows_gitlab_authenticity.png "GitLab domain is not know... yet"

[branch_management]: https://nvie.com/posts/a-successful-git-branching-model/ "A successful Git branching model"
[semantic_versioning]: https://semver.org/ "Semantic Versioning 2.0.0"
